import { CategoryManagementComponent } from './category-management/category-management.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { ManagementComponent } from './management/management.component';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'management', component: ManagementComponent },
    { path: 'post', component: CreatePostComponent },
    { path: 'category', component: CategoryManagementComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }