import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommentService } from './service/comment.service';
import { CategoryService } from './service/category.service';
import { UserService } from './service/user.service';
import { PostService } from './service/post.service';
import { JsonService } from './service/json-service.service';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ManagementComponent } from './management/management.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { FieldErrorDisplayComponent } from './field-error-display/field-error-display.component';
import { CategoryManagementComponent } from './category-management/category-management.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ManagementComponent,
    CreatePostComponent,
    FieldErrorDisplayComponent,
    CategoryManagementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [JsonService, PostService, UserService, CategoryService, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
