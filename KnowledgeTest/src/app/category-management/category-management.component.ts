import { Category } from './../model/category';
import { CategoryService } from './../service/category.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.css']
})
export class CategoryManagementComponent implements OnInit {

  category = new Category();
  form: FormGroup;
  errorMessage = '';
  categories: Category[] = [];

  constructor(private formBuilder: FormBuilder, private categoryService: CategoryService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      name: [null, Validators.required]
    });
  }

  save() {
    if(this.validForm()) {
      if(!this.categoryService.categoryExists(this.category)) {
        this.insertCategory();
        this.category = new Category();
        this.categories = this.categoryService.categories;
      } else {
        this.errorMessage = 'Category already exists';
      }
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  insertCategory() {
    this.categoryService.insertCategory(this.category);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validForm() {
    return this.form.valid;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
