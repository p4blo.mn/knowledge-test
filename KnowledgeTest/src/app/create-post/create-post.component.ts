import { CategoryService } from './../service/category.service';
import { Category } from './../model/category';
import { Post } from './../model/post';
import { PostService } from './../service/post.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  form: FormGroup;
  post: Post = new Post(0, 0, '','','',0,[],null,[]);
  categories: Category[] = [];
  postCategory: string;

  constructor(private formBuilder: FormBuilder, 
    private postService: PostService, 
    private route: Router,
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.categories = this.categoryService.categories;

    this.categories.forEach(ctg => {
      console.log("ctg: " + ctg.name);
      
    })

    this.form = this.formBuilder.group({
      title: [null, Validators.required],
      subtitle: [null, Validators.required],
      body: [null, Validators.required],
      category: [null, Validators.required]
    });
  }

  save() {
    if(this.validForm()) {      
      this.createpost();
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  createpost() {
      this.post.date = Date.now();
      this.post.category = new Category(this.postCategory);
      console.log("Category: " + this.postCategory);
      
      this.post.userId = 1; //Just for test purpose. As the admin user is not registered, the userId of the post will be 1.
      this.postService.insertPost(this.post);

      this.route.navigate(['']);
  }

  isFieldValid(field: string) {
    return !this.form.get(field).valid && this.form.get(field).touched;
  }

  displayFieldCss(field: string) {
    return {
      'has-error': this.isFieldValid(field),
      'has-feedback': this.isFieldValid(field)
    };
  }

  validForm() {
    return this.form.valid;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      console.log(field);
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
