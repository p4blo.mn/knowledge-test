import { UserService } from './../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  login: string = "";
  senha: string = "";
  logado: boolean = false;
  mostrarModal: boolean = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  logar() {
    if(this.login != "admin" || this.senha != "K5uGP8m") {
      this.mostrarModal = true;
    } else {
      this.logado = true;
      this.userService.loggedUser = this.login;
    }
  }

  dismissModal() {
    this.mostrarModal = false;
  }

  deslogar() {
    this.logado = false;
    this.userService.loggedUser = null;
  }

  managementPage() {
    this.router.navigate(['management']);
  }
  
}
