import { CommentService } from './../service/comment.service';
import { UserService } from './../service/user.service';
import { PostService } from './../service/post.service';
import { JsonService } from './../service/json-service.service';
import { Post } from './../model/post';
import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { Comment } from '../model/comment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  commentsByPostId: Comment[] = [];
  images: string[] = [];
  url: string = 'https://jsonplaceholder.typicode.com/';

  constructor(
    private jsonService: JsonService,
    private postService: PostService,
    private userService: UserService,
    private commentService: CommentService
  ) { }

  ngOnInit() {
    this.getImages();
  }

  getUser(userId: number) {
    return this.userService.getUserById(userId);
  }

  getComments(postId: number) {
    return this.commentService.getCommentsByPostId(postId);
  }

  setCommentsByPostId(postId: number) {
    this.commentsByPostId = this.commentService.getCommentsByPostId(postId);
  }

  getImages() {
    this.jsonService.getContent(this.url + 'photos').subscribe(photos => {
      photos.forEach(photo => {
        this.images.push(photo.url);
      });
    })
  }

  getImagesByIndex(index: number) {
    let imageList: string[] = [];

    for(let i = index; i < index+50; i++) {
      imageList.push(this.images[i]);
    }
    return imageList;
  }


}
