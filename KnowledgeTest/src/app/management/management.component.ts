import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }

  createPost() {
    this.route.navigate(['post']);
  }

  categoryManagement() {
    this.route.navigate(['category']);
  }

}
