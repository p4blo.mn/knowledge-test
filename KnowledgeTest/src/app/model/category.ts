export class Category {
    name: string;

    constructor(name?: string) {
        if (name != undefined) {
            this.name = name;
        }
    }
}