export class Comment {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
    ip: string;
    date: Date;
}