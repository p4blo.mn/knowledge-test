import { Comment } from './comment';
import { Category } from './category';
export class Post {
    id: number;
    userId: number;
    title: string;
    subtitle: string;
    body: string;
    date: number;
    images: string[];
    category: Category;
    comments: Comment[];

    constructor(id?: number, userId?: number, title?: string, subtitle?: string, 
        body?: string, date?: number, images?: string[], category?: Category, comments?: Comment[]) {
            if(id != undefined)
                this.id = id;
            if(userId != undefined)
                this.userId = userId;
            if(title != undefined)
                this.title = title;
            if(subtitle != undefined)
                this.subtitle = subtitle;
            if(body != undefined)
                this.body = body;
            if(date != undefined)
                this.date = date;
            if(images != undefined)
                this.images = images;
            if(category != undefined)
                this.category = category;
            if(comments != undefined)
                this.comments = comments;

    }
}