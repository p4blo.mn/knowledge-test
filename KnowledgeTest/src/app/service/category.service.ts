import { Category } from './../model/category';
import { Injectable } from "@angular/core";

@Injectable()
export class CategoryService {
    categories: Category[] = [];

    constructor() {}

    insertCategory(category: Category) {
        console.log("categories.length:" + this.categories.length);
        this.categories.forEach(ctg => {
            console.log(ctg.name);
        })
        
        this.categories.push(category);
    }

    categoryExists(category: Category) {
        this.categories.forEach(ctg => {
            if(category.name === ctg.name) {
                return true;
            }
        })

        return false;
    }

}