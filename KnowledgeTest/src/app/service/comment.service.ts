import { Comment } from './../model/comment';
import { JsonService } from './json-service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class CommentService {
    commentList: Comment[] = [];
    url: string = 'https://jsonplaceholder.typicode.com/';

    commentsByPostId: Comment[] = [];

    constructor(private jsonService: JsonService) {
        jsonService.getContent(this.url + 'comments').subscribe(comments => {
            this.commentList = comments;
        })
    }

    getCommentsByPostId(postId: number) {
        let comments: Comment[] = [];

        this.commentList.forEach(comment => {
            if(comment.postId == postId) {
                comments.push(comment);
            }
        })

        return comments;
    }

    setCommentsByPostId(comments: Comment[]) {
        this.commentsByPostId = comments;
    }
}