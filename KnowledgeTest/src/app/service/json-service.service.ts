import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class JsonService {
  myHeaders = new Headers();

  constructor(private http: Http) {
    this.myHeaders.set('Content-Type', 'application/json');
    this.myHeaders.set('Accept', 'application/json');
  }

  getContent(url: string): Observable<any> {
    let options = new RequestOptions({headers: this.myHeaders});
    return this.http.get(url, options).map(this.extractData).catch(this.handleError);
  }

  getSpecificContent(url: string, data: string[]): Observable<any> {
    if (data.length % 2 == 0) {

      let myParams = new URLSearchParams();
      for (var i = 0; i < data.length; i+=2) {
        myParams.set(data[i], data[i+1]);
      }

      let options = new RequestOptions({ headers: this.myHeaders, params: myParams });

      return this.http.get(url, options).map(this.extractData).catch(this.handleError);
    }
    else {
        console.log('Not enough data to recover JSON.')
        return null;
    }
  }

  insertContent<T>(url:string, data:T): Promise<number> {
    let options = new RequestOptions({headers: this.myHeaders})
    return this.http.post(url, data, options).toPromise().then(res=>res.status).catch(e => 0)
  }

  updateContent<T>(url: string, data: T): Promise<number> {
    let options = new RequestOptions({ headers: this.myHeaders });

    return this.http.put(url, data, options).toPromise().then(res=>res.status).catch(e => 0)
  }

  deleteContent(url: string): Promise<number> {
    let options = new RequestOptions({ headers: this.myHeaders });

    return this.http.delete(url, options).toPromise().then(res => res.status).catch(e => 0);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body;
  }

  private handleError (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

}
