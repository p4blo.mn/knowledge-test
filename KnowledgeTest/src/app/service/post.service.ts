import { JsonService } from './json-service.service';
import { Post } from './../model/post';
import { Injectable } from '@angular/core';
@Injectable()
export class PostService {

  posts: Post[] = [];
  //photoMap = new Map();
  images: string[] =[];
  url: string = 'https://jsonplaceholder.typicode.com/';

  constructor(private jsonService: JsonService) {
    this.getPosts();
    this.getImages();
    //this.fillPhotosByUserId();
  }

  getImages() {
    this.jsonService.getContent(this.url + 'photos').subscribe(photos => {
      photos.forEach(photo => {        
        this.images.push(photo.url);
      });
    })
  }

/* Fill the map with the user Id and the respective photos

   fillPhotosByUserId() {
    this.jsonService.getContent(this.url + 'users').subscribe(users => {
      users.forEach(user => {
        let photoList: string[] = [];

        this.jsonService.getContent(this.url + 'albums?userId=' + user.id).subscribe(albuns => {
          albuns.forEach(album => {
            this.jsonService.getContent(this.url + 'photos?albumId=' + album.id).subscribe(photos => {
              photos.forEach(photo => {
                photoList.push(photo.url);
              });
            })
          });
        })

        this.photoMap.set(user.id, photoList);
        console.log("PhotoList of user " + user.id);
        photoList.forEach(photo => {
          console.log(photo);
        })
      });
    })
  } */


  getPosts() {
    this.jsonService.getContent(this.url + 'posts').subscribe(postList => {
      this.posts = postList;
    })
  }

  insertPost(post: Post) {
    this.posts.push(post);
  }

}