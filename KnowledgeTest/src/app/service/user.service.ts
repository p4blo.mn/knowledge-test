import { User } from './../model/user';
import { JsonService } from './json-service.service';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {

    userList: User[] = []
    url: string = 'https://jsonplaceholder.typicode.com/';
    loggedUser: string;

    constructor(private jsonService: JsonService) {
        this.getUsers();
    }

    getUsers() {
        this.jsonService.getContent(this.url + 'users').subscribe(users => {
            this.userList = users;
        });
    }

    getUserById(userId: number): string {

        let username = '';

        this.userList.forEach(user => {
            if(user.id == userId) {
                username = user.name;
            }
        })

        return username;
    }
}