Para executar o projeto:

$ cd knowledge-test-master/KnowledgeTest

$ npm install

$ ng serve --open

A página inicial mostra a lista de posts da API.

Informar o login admin e a senha K5uGP8m na barra superior e clicar em Login. Ao informar essas credenciais, o sistema habilita o menu "Management" ao lado do login. Caso seja digitado um login ou senha incorretos, o sistema emite uma mensagem informando isso.

Ao clicar em Management, o sistema exibe uma página com 2 botões: Create Post para criar um post e Category Management para gerenciar as categorias. 

Ao clicar em Category Management, outra página é exibida com o botão Create Category. Ao clicar em Create Category, o sistema exibe o input do nome da categoria e um botão de salvar. Ao salvar, a categoria é exibida numa lista abaixo do input.

Ao clicar em Create Post, o sistema exibe uma página com um formulário para preenchimento. Ao preencher todos os dados e clicar em Save, o sistema exibe a página inicial. Caso algum dado do post esteja faltando, o sistema exibe uma mensagem de erro ao clicar em Save.

Após criar um post, a página inicial exibe o post na parte de baixo. 

A página inicial não mostra todas as imagens. Coloquei pra mostrar 3 imagens por post. 

Sobre os posts:
- Cada post exibe a quantidade de comentários que existe para o post. Ao clicar no número de comentários, um modal exibe os dados dos comentários.
- Ao criar um post, ele é cadastrado para o usuário Leanne Graham.
- O post só mostra a data de cadastro, categoria e subtítulo para os posts cadastrados já que os posts da API não tem esses dados.

Como não existe endpoint para cadastro de categorias e o POST da API não funciona, os dados criados são persistidos na memória e só são exibidos enquanto o servidor estiver executando. Caso o servidor seja reiniciado, os dados são perdidos.